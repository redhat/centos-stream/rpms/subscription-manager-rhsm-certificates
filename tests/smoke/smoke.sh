#!/bin/bash

set -e
set -o pipefail

# Check that both PEM files are installed and these files are really PEM files
file /etc/rhsm/ca/redhat-entitlement-authority.pem | grep -q "/etc/rhsm/ca/redhat-entitlement-authority.pem: PEM certificate"
file /etc/rhsm/ca/redhat-uep.pem | grep -q "/etc/rhsm/ca/redhat-uep.pem: PEM certificate"

